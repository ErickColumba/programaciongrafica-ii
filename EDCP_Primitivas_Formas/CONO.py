"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de figuras 3d en este caso un cono  """



from OpenGL.GL import *
from OpenGL.GLU import *



def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)


    gluCylinder(gluNewQuadric(), 1, 0, 3, 10, 10);
    glPopMatrix()