"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de figuras 3d en este caso un cubo se le aplica una textura para cher purebas 
del uso de las mismas """


from OpenGL.GL import *
from OpenGL.GLU import *
import pygame

def loadTexture():

    textureSurface = pygame.image.load("ladrillo.jpg")
    textureData = pygame.image.tostring(textureSurface, "RGBA", 1)
    width = textureSurface.get_width()
    height = textureSurface.get_height()

    glEnable(GL_TEXTURE_2D)
    texid = glGenTextures(1)

    glBindTexture(GL_TEXTURE_2D, texid)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
    0, GL_RGBA, GL_UNSIGNED_BYTE, textureData)

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

    return texid

def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x, y, z)
    glRotatef(rx, 1, 0, 0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w, h, p)
    glColor3f(r, g, b)

    glBegin(GL_QUADS)

    # glColor3f(r - 0.2, g - 0.3, b - 0.2)
    glTexCoord2f(1, -1)
    glVertex3f(1.000000, 1.000000, -1.000000)
    # glColor3f(r, g, b)
    glTexCoord2f(-1, -1)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    # glColor3f(r - 0.2, g - 0.3, b - 0.2)
    glTexCoord2f(-1, 1)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    # glColor3f(1, 1, 1)
    glTexCoord2f(1, 1)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glEnd()

    glNormal3f(0, -1, 0)
    glBegin(GL_QUADS)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()
    glNormal3f(1, 0, 0)
    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glEnd()
    glNormal3f(0, -1, 0)
    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()
    glNormal3f(1, 0, 0)
    glBegin(GL_QUADS)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glEnd()
    glNormal3f(0, 0, -1)
    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glEnd()
    glPopMatrix()

