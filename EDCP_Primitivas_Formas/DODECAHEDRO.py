"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de figuras 3d en este caso un dodecahedro  """



from OpenGL.GL import *
from OpenGL.GLU import *



def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x, y, z)
    glRotatef(rx, 1, 0, 0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w, h, p)
    #glScalef(0.005, 0.005, 0.005)

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glColor3f(0, g, 0)
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glColor3f(0, 0, b)
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glColor3f(0, g, 0)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glColor3f(r, 0, 0)
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3

    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glColor3f(0, 0, b)
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glColor3f(0, g, 0)
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glColor3f(0, 0, b)
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glColor3f(r, 0, 0)
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glColor3f(0, 0, b)
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glColor3f(0, g, 0)
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glColor3f(0, 0, b)
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glColor3f(r, 0, 0)
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glColor3f(0, 0, b)
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glColor3f(0, g, 0)
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glColor3f(0, 0, b)
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glColor3f(r, 0, 0)
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glColor3f(0, 0, b)
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glColor3f(0, g, 0)
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glColor3f(0, 0, b)
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glColor3f(r, 0, 0)
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glColor3f(0, 0, b)
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glColor3f(0, g, 0)
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glColor3f(0, 0, b)
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glColor3f(r, 0, 0)
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glColor3f(0, 0, b)
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glColor3f(0, g, 0)
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glColor3f(0, 0, b)
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glColor3f(r, 0, 0)
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17

    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glColor3f(0, 0, b)
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glColor3f(0, g, 0)
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glColor3f(0, 0, b)
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glColor3f(r, 0, 0)
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glColor3f(0, g, 0)
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glColor3f(0, 0, b)
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glColor3f(r, 0, 0)
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glColor3f(0, 0, b)
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glColor3f(0, g, 0)
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glColor3f(0, 0, b)
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glColor3f(r, 0, 0)
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glColor3f(0, g, 0)
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glColor3f(0, g, 0)
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glColor3f(0, 0, b)
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glColor3f(r, 0, 0)
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glEnd()

    glBegin(GL_POLYGON)
    glColor3f(r, 0, 0)
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glColor3f(0, 0, b)
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glColor3f(0, g, 0)
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glColor3f(0, 0, b)
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glColor3f(r, 0, 0)
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glEnd()

    # dibujo del contorno del dodecadro
    glColor3b(r, g, b)
    glLineWidth(0.1)

    glBegin(GL_LINE_LOOP)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3

    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17

    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glEnd()

    glBegin(GL_LINE_LOOP)
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glEnd()
    glPopMatrix()