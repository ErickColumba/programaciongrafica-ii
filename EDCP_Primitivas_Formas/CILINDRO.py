"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de figuras 3d en este caso un cilindro  """


from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
import math



def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)

    glBegin(GL_QUAD_STRIP)
    for i in np.arange(0, 2.01 * math.pi, 0.1):
        auxX = math.sin(i) * 3
        auxZ = math.cos(i) * 3
        glColor(i / 7, 7 / (i + 0.1), i / 7, 1)
        glVertex(auxX, 0, auxZ)
        glColor(i / 7, 0.2, 7 / (i + 0.1), 1)
        glVertex(auxX, h, auxZ)
    glEnd()
    glPopMatrix()

