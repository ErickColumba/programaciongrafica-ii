"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Clase para la creacion de una pantalla donde se muestran las diferentes figuras 3D  """




#Import OpenGL and GLU.  Don't import GLUT because it is ancient, broken, inflexible, and poorly
#designed--and we aren't using it.
from OpenGL.GL import *
from OpenGL.GLU import *

#Import PyGame.  We'll mostly just use this to make a window.  Also import all the local
#declarations (e.g. pygame.KEYDOWN, etc.), so that we don't have to keep typing "pygame." in front
#of everything.  E.g., now we can do "KEYDOWN" instead of "pygame.KEYDOWN".
import pygame
from pygame.locals import *
import ICOSAEDRO as icosaedro
import CUBO as cubo
import PIRAMIDECUADRADA as piramideCuadrada
import PRISMA as prisma
import  ESFERA as esfera
import  CONO as cono
import OCTAHEDRO as octaedro
import CILINDRO as cilindro
import DODECAHEDRO as dodecahedro
import LUCES as luces

import sys, os, traceback




if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"

from math import *

pygame.display.init()
pygame.font.init()


db=0
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)



pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)
glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center


def get_input():
    global camera_rot, camera_radius

    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    #Check how much the mouse moved since you last called this function.
    mouse_rel = pygame.mouse.get_rel()
    #List all the events that happened.
    for event in pygame.event.get():
        #Clicked the little "X"; close the window (return False breaks the main loop).
        if   event.type == QUIT: return False
        #If the user pressed a key:
        elif event.type == KEYDOWN:
            #If the user pressed the escape key, close the window.
            if   event.key == K_ESCAPE or event.key == pygame.K_RETURN: return False
        #If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9


    #If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def draw():
    global db
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]

    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    # cambiar de figura
    keypress = pygame.key.get_pressed()

    if keypress[pygame.K_1]:
        db = 1
    if keypress[pygame.K_2]:
        db = 2
    if keypress[pygame.K_3]:
        db = 3
    if keypress[pygame.K_4]:
        db = 4
    if keypress[pygame.K_5]:
        db = 5
    if keypress[pygame.K_6]:
        db = 6
    if keypress[pygame.K_7]:
        db = 7
    if keypress[pygame.K_8]:
        db = 8
    if keypress[pygame.K_9]:
        db = 9

   #Dibujar Figuras
#    luz.IniciarIluminacion()


    if(db==1):
        pygame.display.set_caption("CUBO")
        cubo.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,0,0)

    if (db == 2):
        pygame.display.set_caption("PIRAMIDE CUADRADA")
        piramideCuadrada.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,0)
        
    if (db == 3):
        pygame.display.set_caption("PRISMA")
        prisma.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
    if (db == 4):
        pygame.display.set_caption("ESFERA")
        esfera.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
    if (db == 5):
        pygame.display.set_caption("CONO")
        cono.draw(0,0,0,0.2,0.2,0.2,90,180,90,0,1,1)
    if (db == 6):
        pygame.display.set_caption("CILINDRO")
        cilindro.draw(0,0,0,0.2,1,0.2,0,0,0,1,1,1)
    if (db == 7):
        pygame.display.set_caption("ICOSAEDRO")
        icosaedro.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
        icosaedro.IcosaedroWireframe(0,0,0,0.2,0.2,0.2,0,0,0,1,0,1)
    if (db == 8):
        pygame.display.set_caption("OCTAEDRO")
        octaedro.draw(0,0,0,0.2,0.2,0.2,0,0,0,0,1,1)
    if (db == 9):
        pygame.display.set_caption("DODECAEDRO")
        dodecahedro.draw(0,0,0,0.01,0.01,0.01,0,0,0,1,1,0)
    glColor3f(1,1,1)
    glBegin(GL_LINES)
    #Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1,0,0)
    #Make two vertices, thereby drawing a (red) line.
    glVertex(-10,0,0); glVertex3f(10,0,0)
    #Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0,1,0)
    #Make two vertices, thereby drawing a (green) line.
    glVertex(0,-10,0); glVertex3f(0,10,0)
    #Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0,0,10)
    #Make two vertices, thereby drawing a (blue) line.
    glVertex(0,0,-10); glVertex3f(0,0,10)

    glColor3f(1,1,1)
    #We're done drawing lines; tell OpenGL so.
    glEnd()


    pygame.display.flip()


def main():

    clock = pygame.time.Clock()
    while True:
        if not get_input(): break

        draw()
        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()
if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()
