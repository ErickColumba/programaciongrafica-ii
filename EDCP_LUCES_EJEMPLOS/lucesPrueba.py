"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de diferentes luces haciendo uso de la libreria GLLIB  """

from OpenGL.GL import *
from OpenGL.GLU import *
import CUBO as cubo
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *
from OpenGL.GL import *
from glLib.glLibLight import glLibLight
from glLib.glLibLocals import GLLIB_POINT_LIGHT
from glLib.glLibMath import normalize




if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"



pygame.display.init()
pygame.font.init()


db=0
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)



pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)
glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
glEnable(GL_DEPTH_TEST)


camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center


def get_input():
    global camera_rot, camera_radius

    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    #Check how much the mouse moved since you last called this function.
    mouse_rel = pygame.mouse.get_rel()
    #List all the events that happened.
    for event in pygame.event.get():
        #Clicked the little "X"; close the window (return False breaks the main loop).
        if   event.type == QUIT: return False
        #If the user pressed a key:
        elif event.type == KEYDOWN:
            #If the user pressed the escape key, close the window.
            if   event.key == K_ESCAPE or event.key == pygame.K_RETURN: return False
        #If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9


    #If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def draw():
    global db
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]

    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    keypress = pygame.key.get_pressed()


    if keypress[pygame.K_1]:
        db = 1
    if keypress[pygame.K_2]:
        db = 2
    if keypress[pygame.K_3]:
        db = 3
    if keypress[pygame.K_4]:
        db = 4

    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_TEXTURE_2D)

    if (db == 1):
        #luces.luz1()
        Light1 = glLibLight(1)
        Light1.set_pos([0, 0.5, 0])
        Light1.set_type(GLLIB_POINT_LIGHT)
        Light1.set_atten(0.0, 0.0, 0.0)
        Light1.enable()
       # Light1.set_ambient([1,1,1])
        Light1.draw_as_point()
        cubo.draw(0, 0, 0, 0.2, 0.2, 0.2, 0, 0, 0, 1, 0, 0)
    if (db == 2):
        Light2 = glLibLight(2)
        Light2.set_pos([0, 0.4, 0])
        Light2.set_type(GLLIB_POINT_LIGHT)
        Light2.set_diffuse([0, 0, 1])
        Light2.set_specular([0, 0, 1])
        Light2.set_atten(1.0, 0.0, 1.0)
        Light2.enable()
        Light2.set()
        Light2.draw_as_point()
        cubo.draw(0, 0, 0, 0.2, 0.2, 0.2, 0, 0, 0, 1, 0, 0)
    if (db == 3):
        Light3 = glLibLight(3)
        Light3.set_pos([0, 0.3, 0])
        Light3.set_type(GLLIB_POINT_LIGHT)
        Light3.set_diffuse([0, 1, 0])
        Light3.set_specular([0, 1, 0])
        Light3.set_atten(1.0, 1.0, 1.0)
        Light3.set_spot_dir(normalize([0, -1, 0]))
        Light3.set_spot_angle(45)
        Light3.set_spot_ex(1)
        Light3.enable()
        Light3.set()
        # Light3.draw_as_point()
        Light3.draw_as_sphere()
        cubo.draw(0, 0, 0, 0.2, 0.2, 0.2, 0, 0, 0, 1, 0, 0)
    if (db == 4):
        Light4 = glLibLight(4)
        Light4.set_pos([0, 0.5, 0])
        Light4.set_type(GLLIB_POINT_LIGHT)
        Light4.set_diffuse([1, 0, 0])
        Light4.set_specular([1, 0, 0])
        Light4.set_atten(1.0, 0.0, 1.0)
        Light4.set_spot_dir(normalize([-0.4, -1.0, 1.0]))
        Light4.set_spot_angle(45.0)
        Light4.set_spot_ex(0.5)
        Light4.enable()
        Light4.set()
        Light4.draw_as_sphere()
        cubo.draw(0, 0, 0, 0.2, 0.2, 0.2, 0, 0, 0, 1, 0, 0)






    glColor3f(1,1,1)
    glBegin(GL_LINES)
    #Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1,0,0)
    #Make two vertices, thereby drawing a (red) line.
    glVertex(-10,0,0); glVertex3f(10,0,0)
    #Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0,1,0)
    #Make two vertices, thereby drawing a (green) line.
    glVertex(0,-10,0); glVertex3f(0,10,0)
    #Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0,0,10)
    #Make two vertices, thereby drawing a (blue) line.
    glVertex(0,0,-10); glVertex3f(0,0,10)

    glColor3f(1,1,1)
    #We're done drawing lines; tell OpenGL so.
    glEnd()


    pygame.display.flip()


def main():

    clock = pygame.time.Clock()
    while True:
        if not get_input(): break

        draw()
        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()
if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()
