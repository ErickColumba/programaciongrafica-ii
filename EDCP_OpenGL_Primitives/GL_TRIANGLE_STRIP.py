"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de un poligono hacieno uso del GL_TRIANGLE_STRIP """


from OpenGL.GL import *
from OpenGL.GLU import *



def dibujarTS():
    glNormal3d(1, 0, 0);
    glBegin(GL_TRIANGLE_STRIP)
    glColor3f(0, 0, 1)
    glVertex3f(0.0,1.0,0.0)
    glColor3f(0, 0, 1)
    glVertex3f(0.0,0.0,0.0)
    glColor3f(0, 0, 1)
    glVertex3f(1.0,1.0,0.0)
    glColor3f(0, 1, 0)
    glVertex3f(1.0, 0.0, 0.0)
    glColor3f(0, 1, 0)
    glVertex3f(2.0, 1.0, 0.0)

    glEnd()
