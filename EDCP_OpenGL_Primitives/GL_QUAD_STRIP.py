"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de un poligono hacieno uso del G_QUAD_STRIP """

from OpenGL.GL import *
from OpenGL.GLU import *



def dibujarQS():

    glBegin(GL_QUAD_STRIP)
    glColor3f(1, 0, 0)
    glVertex3f(0.0, 1.0, 0.5)
    glColor3f(1, 0, 0)
    glVertex3f(0.0, 0.0, 0.5)
    glColor3f(1, 0, 0)
    glVertex3f(1.0, 1.0, 0.5)
    glColor3f(1, 0, 0)
    glVertex3f(1.0, 0.0, 0.5)
    glColor3f(0, 0, 1)
    glVertex3f(2.0, 1.0, 0.5)
    glColor3f(0, 0, 1)
    glVertex3f(2.0, 0.0, 0.5)


    glEnd()