"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de un poligono hacieno uso del G_QUAD """


from OpenGL.GL import *
from OpenGL.GLU import *

def dibujarQ():
    glColor3f(1, 0, 1)
    glBegin(GL_QUADS)
    glVertex3f(-1.0,1.0,0.0)
    glVertex3f(-1.0,0.0,0.0)
    glVertex3f(0.0,0.0,0.0)
    glVertex3f(0.0, 1.0, 0.0)

    glEnd()