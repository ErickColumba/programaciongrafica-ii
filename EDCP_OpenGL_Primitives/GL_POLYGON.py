"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de un poligono haciendo uso de GL_Polygon """

from OpenGL.GL import *
from OpenGL.GLU import *

def dibujarP():
    glColor3f(1, 1, 0)
    glBegin(GL_POLYGON)
    glVertex3f(-1.0,1.0,0.5)
    glVertex3f(-2.0, 0.0, 0.5)
    glVertex3f(1.0,0.0,0.5)
    glVertex3f(2.0,0.5,0.5)
    glVertex3f(2.0, 1.0, 0.5)
    glVertex3f(0.0, 1.0, 0.5)

    glEnd()