"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Creacion de un poligono hacieno uso del GL_TRIANGLE """

from OpenGL.GL import *
from OpenGL.GLU import *



def dibujarT():

    glNormal3d(1, 0, 0);

    glBegin(GL_TRIANGLES)
    glColor3f(1, 0, 0)
    glVertex3f(0.0,1.0,0.0)
    glColor3f(0, 1, 0)
    glVertex3f(0.0,0.0,0.0)
    glColor3f(0, 0, 1)
    glVertex3f(1.0,1.0,1.0)
    glEnd()