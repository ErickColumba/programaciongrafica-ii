"""Universidad Central del Ecuador--Computacion Grafica--Erick David Columba Parraga """
"""Primera version del Proyecto final donde se muestra un escenario con miramides con textura"""

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *

from pyglet.gl import glPushMatrix

import primitivas3D
import random

if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"] = "1"

pygame.display.init()
pygame.font.init()

screen_size = [800, 600]

pygame.display.set_mode(screen_size, OPENGL | DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [90.0, 5.0]  # The spherical coordinates' angles (degrees).
camera_radius = 3.0  # The sphere's radius
camera_center = [0.0, 0.0, 0.0]  # The sphere's center

velocidad = 10
posicionInicialZ = - 50
posicionesInicialX = []
posicionesZ = []
obstaculos = []
spawnRate = .0025
tiempoEntreObstaculos = 1 / spawnRate

class piramide:
    def __init__(self, size):
        self.size = size

    def draw_piramide(self):
        primitivas3D.draw_piramidecuadrada(self.size)


def get_input():
    global camera_rot, camera_radius, posXPlayer
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]

    #movimiento jugador
    pressed_keys = pygame.key.get_pressed()
    if pressed_keys[pygame.K_RIGHT] or pressed_keys[pygame.K_d]:
        posXPlayer += 0.05
    if pressed_keys[pygame.K_LEFT] or pressed_keys[pygame.K_a]:
        posXPlayer -= 0.05
    #salto
    #if pressed_keys[pygame.K_SPACE]:
        #
    return True


def setup_draw():
    # Clear the screen's color and depth buffers so we have a fresh space to draw geometry onto.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Setup the viewport (the area of the window to draw into)
    glViewport(0, 0, screen_size[0], screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )


def draw():
    setup_draw()
    # Start drawing lines.  Each subsequent pair of glVertex*() calls will draw one line.
    glBegin(GL_LINES)
    # Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1, 0, 0)
    # Make two vertices, thereby drawing a (red) line.
    glVertex(0, 0, 0);
    glVertex3f(1, 0, 0)
    # Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0, 1, 0)
    # Make two vertices, thereby drawing a (green) line.
    glVertex(0, 0, 0);
    glVertex3f(0, 1, 0)
    # Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0, 0, 1)
    # Make two vertices, thereby drawing a (blue) line.
    glVertex(0, 0, 0);
    glVertex3f(0, 0, 1)
    glEnd()

    glColor3f(1, 1, 1)
    glBegin(GL_QUADS)
    glVertex3f(2, 0, 100)
    glVertex3f(-2, 0, 100)
    glVertex3f(-2, 0, -100)
    glVertex3f(2, 0, -100)
    glEnd()

    draw_jugador()

    draw_obstaculos()

    pygame.display.flip()


posXPlayer = 0


def draw_jugador():
    global posXPlayer
    glPushMatrix()
    glTranslatef(posXPlayer, 0.25, 0)
    glScalef(0.25, 0.25, 0.25)
    primitivas3D.draw_cubo(1, 0, 0)
    glPopMatrix()

def draw_obstaculos():
    global deltaTime, velocidad, t, tiempoEntreObstaculos, posicionesZ
    # dibujar obstaculos
    for i in range(0, len(obstaculos)):
        glPushMatrix()
        glTranslatef(posicionesInicialX[i], 0.2, posicionesZ[i])
        obstaculos[i].draw_piramide()
        glPopMatrix()
    # mover obstaculos
    for i in range(0, len(posicionesZ)):
        posicionesZ[i] += velocidad * deltaTime
    # instanciar obstaculos
    if t > tiempoEntreObstaculos:
        obstaculos.append(piramide(0.5))
        posicionesZ.append(posicionInicialZ)
        posicionesInicialX.append(random.random() * 4 - 2)
        tiempoEntreObstaculos = t + 1 / spawnRate
    # eliminar obstaculos lejanos
    for i in range(0, len(posicionesZ)):
        if(posicionesZ[i] > 10):
            posicionesZ.pop(i)
            obstaculos.pop(i)
            posicionesInicialX.pop(i)
            break


tiempoInicial = 0
timepoFinal = 0
deltaTime = 0
t = 0


def main():
    global tiempoInicial, tiempoFinal, deltaTime, t
    clock = pygame.time.Clock()
    tiempoInicial = pygame.time.get_ticks()

    obstaculos.append(piramide(0.5))
    posicionesInicialX.append(random.random() * 4 - 2)
    posicionesZ.append(posicionInicialZ)

    while True:
        deltaTimeI = t
        if not get_input(): break
        setup_draw()

        draw()
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.

        tiempoFinal = pygame.time.get_ticks()
        t = tiempoFinal - tiempoInicial
        deltaTime = t - deltaTimeI
        deltaTime = deltaTime / 1000
        fps = 1 / (deltaTime)
        print("fps ", fps)
        print("delta time ", deltaTime)
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()



